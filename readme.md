## Group 14
#### Xiao Hu ID: 1471944
#### Yang Dong ID:1496348
#### Zhaokai Tian ID: 1501378

# Installation Guide

## Requirements: 
 - Server: Ubuntu 14.04 or higher
 - Device: Raspbery Pi 3 Model B+
 - Hostname: mqtt.example.co.nz
 - Server HTTP code

## Installation

###  MQTT Installation and Configuration: 
install MQTT (Mosquitto)

    $ sudo apt-get install mosquitto mosquitto-clients

### Install Certbot

    $ sudo add-apt-repository ppa:certbot/certbot
    $ sudo apt-get update
    $ sudo apt-get install certbot

### Runing Certbot

	$ sudo ufw allow http

	'''output'''
	Rule added
	
### Setting up Certbot Automatic Renewals
After type the command choose your favorate editer

	sudo crontab -e

```
. . .
15 3 * * * certbot renew --noninteractive --post-hook "systemctl restart mosquitto"
``` 
Every day at 03:15 in the morning it will renew the sertificate automatically

### Config MQTT username and password
This command will prompt you to enter a password for the specified username, and place the results in `/etc/mosquitto/passwd`.

	$ sudo mosquitto_passwd -c /etc/mosquitto/passwd sammy

open config file and tell MQTT to use username and password

	$ sudo nano /etc/mosquitto/conf.d/default.conf
appand to /etc/mosquitto/conf.d/default.conf

```
allow_anonymous false
password_file /etc/mosquitto/passwd
```
now restart the service

	sudo systemctl restart mosquitto

### Configure MQTT SSL

	$ sudo nano /etc/mosquitto/conf.d/default.conf	

Append following lines
```
. . .
listener 1883 localhost

listener 8883
certfile /etc/letsencrypt/live/mqtt.example.co.nz/cert.pem
cafile /etc/letsencrypt/live/mqtt.example.co.nz/chain.pem
keyfile /etc/letsencrypt/live/mqtt.example.co.nz/privkey.pem
```
Now restart service again

	$ sudo systemctl restart mosquitto
Update firewall

	$ sudo ufw allow 8883
	
### Configure MQTT Over WebSocket

	$ sudo nano /etc/mosquitto/conf.d/default.conf
At the end of line append
```
. . .
listener 8083
protocol websockets
certfile /etc/letsencrypt/live/mqtt.example.co.nz/cert.pem
cafile /etc/letsencrypt/live/mqtt.example.co.nz/chain.pem
keyfile /etc/letsencrypt/live/mqtt.example.co.nz/privkey.pem
```
Save and exit. Restart the service

	$ sudo systemctl restart mosquitto
Add port to firewall

	$ sudo ufw allow 8083

### Get SSL Certificate from  [SSL for Free](https://www.sslforfree.com/)
Follow the instrution and download the Certificates
upload the certificates to  `/etc/ssl/`
### Install Nginx

	$ sudo apt-get install nginx -y

### Configure Nginx
Use HTTPS port instead of HTTP.
Open configure file

	$ sudo nano /etc/nginx/sites-available/default

change following lines
```
	#listen 80 default_server;
    #listen [::]:80 default_server;

    # SSL configuration
    #
    listen 443 ssl default_server;
    listen [::]:443 ssl default_server;

    ssl on;
    ssl_certificate /etc/ssl/certificate.crt;
    ssl_certificate_key /etc/ssl/private.key;
```
Restart the service

	$ sudo service nginx restart
### Setup Web Files
Upload all files in to `/var/www/html/`

Goto your domain and check if it now head to HTTPS

# Usage Documentation
### MQTT Admin

#### Connect to MQTT
- Click Setting Button and Enter all requiremented variables
- After Save button clicked system will automatically connect to your server

#### Publish Topic
- Input Topic and Payload(message)
- Click Publish will push the payload into MQTT
- Click Publish (Retain) will push the payload with topic. And leave the message on Broker. Every new connection will receive the message

#### Subscribe Topic
- type in topic with enter
- start button will keep receiving payloads
- pause button will pause current receiving
- stop button will stop current receiving
- delete button will clear all payloads
- add tab will add a new tab
- double click tab can rename the tab

### Raspberry Pi
#### stick_direction.py
double click the file to open.

#### secondSubscribe.py
- click Pth button
- click Stick button
- click Magneto button
##to subscribe different topics and show on the Application screen
