#! /usr/bin/env python
# coding: utf-8
from signal import pause
import time
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
# import paho.mqtt.subscribe as subscribe
import json
import ssl
import os
try:
    import Tkinter as tk
    from Tkinter import StringVar
except ImportError:
    # for Python3
    import tkinter as tk
    from tkinter import StringVar

import threading
# import my_thread as myThread
# import jobInfo_thread as infoThread


root = tk.Tk()
def on_closing():
  root.destroy()
  # sense.clear()
  os.system("sudo pkill -9 python")
root.protocol("WM_DELETE_WINDOW", on_closing)
def buttonPthClick():
  global msgStr,topicSub
  topicSub="Pth"
  msgStr=1
  loop_connect()


def buttonStickClick():
  global msgStr,topicSub
  topicSub="button"
  msgStr=2 
  loop_connect()

def buttonMagnetoClick():
  global msgStr,topicSub
  topicSub="magneto"
  msgStr=3
  loop_connect()

font10 = "-family {DejaVu Sans} -size 14 -weight normal -slant"  \
  " roman -underline 0 -overstrike 0"
font9 = "-family {DejaVu Sans} -size 16 -weight normal -slant "  \
  "roman -underline 0 -overstrike 0"

root.geometry("571x678+410+14")
root.title("Second Subscribe")
root.configure(highlightcolor="black")

buttonPth = tk.Button(root)
buttonPth.place(relx=0.07, rely=0.03, height=26, width=76)
buttonPth.configure(activebackground="#d9d9d9")
buttonPth.configure(text='''Pth''')
buttonPth.configure(command=buttonPthClick)


buttonStick = tk.Button(root)
buttonStick.place(relx=0.26, rely=0.03, height=26, width=76)
buttonStick.configure(activebackground="#d9d9d9")
buttonStick.configure(text='''stick''')
buttonStick.configure(command=buttonStickClick)

buttonMagneto = tk.Button(root)
buttonMagneto.place(relx=0.45, rely=0.03, height=26, width=76)
buttonMagneto.configure(activebackground="#d9d9d9")
buttonMagneto.configure(text='''Magneto''')
buttonMagneto.configure(command=buttonMagnetoClick)

lb_subscribeTV = StringVar()
Label12 = tk.Label(root)
Label12.place(relx=0.07, rely=0.12, height=200, width=400)
Label12.configure(font=font9)
Label12.configure(text='''subscribe:''')
Label12.configure(textvariable=lb_subscribeTV)
# Label12.pack()




tls = {'cert_reqs': ssl.CERT_REQUIRED, 'tls_version': ssl.PROTOCOL_TLS}
auth = {'username': 'iot', 'password':'test123'}
# msgs = [{'topic': "pth", 'payload': json.dumps(pthValues)}, {'topic': "button", 'payload': json.dumps(buttonValues)}, {'topic': "magneto", 'payload': json.dumps(magnetoValues)}]
hostname = "mqtt.multisupport.co.nz" 
port = 8883
topics = ["pth","button","magneto","command"]
topicSub=""
topicSubscribe=''
msgStr=0
def loop_connect():
  global topicSub, tls, auth, hostname  
  if topicSub != "":
    print(msgStr)
    publish.single(topic="command",payload=msgStr,hostname=hostname,port=port, auth=auth, tls=tls)
    

def subscribeTopic():
  global msgs, tls, auth, hostname, msgStr
  
  # The callback for when a PUBLISH message is received from the server.
  def on_message(client, userdata, msg):
      # print(msg.topic+" "+str(msg.payload))
      # msgStr=str(msg.payload)
      lb_subscribeTV.set(str(msg.payload))
      # sense.show_message(str(msg.payload))
      # define 3 topics widgets set()


  client = mqtt.Client()
  client.on_connect = on_connect
  client.on_message = on_message

  client.tls_set(cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLS)
  client.username_pw_set("iot", password="test123")
  client.connect(host=hostname, port=port, keepalive=240)
  client.loop_forever()
  # Blocking call that processes network traffic, dispatches callbacks and
  # handles reconnecting.
  # Other loop*() functions are available that give a threaded interface and a
  # manual interface.
  
  # m = subscribe.simple(topics, hostname=hostname, retained=False, msg_count=1, port=port, qos=0, client_id="001", auth=auth, tls=tls, keepalive=0)     
  # # sense.show_message(m.payload)
  # print(m.payload)
  # lb_subscribeTV.set(m.payload)

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    global topicSub
    if topicSub !="":
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
      print(topicSub)
      client.subscribe("Pth")
      client.subscribe("button")
      client.subscribe("magneto")

threading.Thread(target=subscribeTopic).start()

root.mainloop()
