#! /usr/bin/env python
# coding: utf-8

from sense_hat import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED
from signal import pause
import time
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
# import paho.mqtt.subscribe as subscribe
import json
import ssl

import os
import Tkinter as tk
from Tkinter import StringVar

import threading





root = tk.Tk()


def on_closing():
  root.destroy()
  sense.clear()
  os.system("sudo pkill -9 python")

root.protocol("WM_DELETE_WINDOW", on_closing)


font10 = "-family {DejaVu Sans} -size 14 -weight normal -slant"  \
  " roman -underline 0 -overstrike 0"
font9 = "-family {DejaVu Sans} -size 16 -weight normal -slant "  \
  "roman -underline 0 -overstrike 0"
root.title("123")
root.geometry("571x678+410+14")
root.title("Gateway UI")
root.configure(highlightcolor="black")


Label12 = tk.Label(root)
Label12.place(relx=0.07, rely=0.12, height=26, width=104)
Label12.configure(font=font9)
Label12.configure(text='''subscribe:''')

lb_subscribeTV = StringVar()
lb_subscribe = tk.Label(root)
lb_subscribe.place(relx=0.07, rely=0.15, height=66, width=600)
lb_subscribe.configure(font=font10)
lb_subscribe.configure(textvariable=lb_subscribeTV)
lb_subscribe.configure(width=1)
lb_subscribeTV.set("none")

Labelframe2 = tk.LabelFrame(root)
Labelframe2.place(relx=0.09, rely=0.25, relheight=0.7
        , relwidth=0.75)
# Labelframe2.configure(relief=GROOVE)
Labelframe2.configure(text='''current status''')
Labelframe2.configure(width=430)

Label1 = tk.Label(Labelframe2)
Label1.place(relx=0.05, rely=0.06, height=30, width=200, y=-11)
Label1.configure(font=font9)
# Label1.configure(justify=LEFT)
Label1.configure(text='''Temperature(C):''')
Label1.configure(width=197)

temperatureTV = StringVar()
temperature = tk.Label(Labelframe2, textvariable=temperatureTV)
# print(temperature.textvariable)
temperature.place(relx=0.6, rely=0.06, height=30, width=96, y=-11)
temperature.configure(font=font9)
# temperature.configure(justify=LEFT)
# temperature.configure(text=temperatureVar.get())
# temperature.configure(text=stick_direction.temperature)
temperature.configure(width=76)
# temperature.pack()
temperatureTV.set("0.00")

Label3 = tk.Label(Labelframe2)
Label3.place(relx=0.05, rely=0.19, height=30, width=200, y=-11)
Label3.configure(font=font9)
# Label3.configure(justify=LEFT)
Label3.configure(text='''Pressure(KPa):''')
Label3.configure(width=166)

pressureTV = StringVar()
pressure = tk.Label(Labelframe2)
pressure.place(relx=0.6, rely=0.19, height=30, width=96, y=-11)
pressure.configure(font=font9)
pressure.configure(textvariable=pressureTV)
pressure.configure(width=60)
pressureTV.set("0.00")

Label5 = tk.Label(Labelframe2)
Label5.place(relx=0.05, rely=0.29, height=30, width=200, y=-11)
Label5.configure(font=font9)
# Label5.configure(justify=LEFT)
Label5.configure(text='''Humidity(%):''')
Label5.configure(width=146)

humidityTV=StringVar()
humidity = tk.Label(Labelframe2)
humidity.place(relx=0.6, rely=0.29, height=30, width=96, y=-11)
humidity.configure(font=font9)
# humidity.configure(justify=LEFT)
humidity.configure(textvariable=humidityTV)
humidityTV.set("0")

Label7 = tk.Label(Labelframe2)
Label7.place(relx=0.05, rely=0.4, height=30, width=200, y=-11)
Label7.configure(font=font9)
# Label7.configure(justify=LEFT)
Label7.configure(text='''Magnetometer:''')
Label7.configure(width=170)

direction = tk.Label(Labelframe2)
direction.place(relx=0.63, rely=0.4, height=38, width=36, y=-11)
direction.configure(width=36)

Label9 = tk.Label(Labelframe2)
Label9.place(relx=0.16, rely=0.55, height=26, width=29, y=-11)
Label9.configure(font=font10)
# Label9.configure(justify=LEFT)
Label9.configure(text='''X:''')

Label10 = tk.Label(Labelframe2)
Label10.place(relx=0.16, rely=0.63, height=26, width=28, y=-11)
Label10.configure(font=font10)
# Label10.configure(justify=LEFT)
Label10.configure(text='''Y:''')

Label11 = tk.Label(Labelframe2)
Label11.place(relx=0.16, rely=0.72, height=26, width=29, y=-11)
Label11.configure(font=font10)
# Label11.configure(justify=LEFT)
Label11.configure(text='''Z:''')

dir_xTV=StringVar()
dir_x = tk.Label(Labelframe2)
dir_x.place(relx=0.49, rely=0.55, height=26, width=200, y=-11)
dir_x.configure(font=font10)
dir_x.configure(textvariable=dir_xTV)
dir_xTV.set("0.00")

dir_yTV=StringVar()
dir_y = tk.Label(Labelframe2)
dir_y.place(relx=0.49, rely=0.63, height=26, width=200, y=-11)
dir_y.configure(font=font10)
dir_y.configure(textvariable=dir_yTV)
dir_yTV.set("0.00")

dir_zTV=StringVar()
dir_z = tk.Label(Labelframe2)
dir_z.place(relx=0.49, rely=0.74, height=26, width=200, y=-11)
dir_z.configure(font=font10)
dir_z.configure(textvariable=dir_zTV)
dir_zTV.set("0.00")

lb_stickTV = StringVar()
lb_stick = tk.Label(Labelframe2)
lb_stick.place(relx=0.6, rely=0.84, height=48, width=156, y=-11)
lb_stick.configure(font=font10)
lb_stick.configure(textvariable=lb_stickTV)
lb_stick.configure(width=156)
lb_stickTV.set("none")


x = 3
y = 3
counter = 0
pressure=''
temperature=''
humidity=''
magnetoX=''
magnetoY=''
magnetoZ=''

sense = SenseHat()
event= sense.stick.get_events()
loop=0
dir = "none"

def clamp(value, min_value=0, max_value=7):
  return min(max_value, max(min_value, value))

def pushed_up(event):
  global y
  #global value_up
  if event.action != ACTION_RELEASED:
    y = clamp(y - 1)
    global dir
    dir="pushed_up"

def pushed_down(event):
  global y
  #global value_down
  if event.action != ACTION_RELEASED:
    y = clamp(y + 1)
    global dir
    dir="pushed_down"
def pushed_left(event):
  global x
  #global values_left
  if event.action != ACTION_RELEASED:
    x = clamp(x - 1)
    global dir
    dir="pushed_left"
def pushed_right(event):
  global x
  if event.action != ACTION_RELEASED:
    x = clamp(x + 1)
    global dir
    dir="pushed_right"

def refresh():
  sense.clear()
  sense.set_pixel(x, y, 255, 255, 255)
  

sense.stick.direction_up = pushed_up
sense.stick.direction_down = pushed_down
sense.stick.direction_left = pushed_left
sense.stick.direction_right = pushed_right
sense.stick.direction_any = refresh
refresh()
msgs=[]
isShow=0
def loop_getInfo():
  global msgs,isShow, counter,temperatureVar,humidityVar,magnetoX,magnetoY,magnetoZ
  while True:
    raw=sense.get_compass_raw()
    kpa = sense.pressure * 0.1
    pressureVar="{:.2f}".format(kpa)
    temperatureVar="{:.2f}".format(sense.temperature)
    humidityVar="{:.0f}".format(sense.humidity)
    magnetoX="{x}".format(**raw)
    magnetoY="{y}".format(**raw)
    magnetoZ="{z}".format(**raw)

    print(sense.compass_raw)
    print("Temperature = %s C" % temperatureVar)
    print("Pressure = %s MPa" % pressureVar)
    print("Humidity = %s " % humidityVar)
    print("time = %s " % "{:.0f} %".format(time.time()))
    print("counter = %s" % counter)
    print('\n')       
           
    pthValues={
               "pth": {
                   "pressure": pressureVar,
                   "temperature": temperatureVar,
                   "humidity": humidityVar
               },
               "timestamp" : "{:.0f}".format(time.time()),
               "counter" : counter
             }
    buttonValues={
               "button": {
                   "direction": dir
               },
               "timestamp" : "{:.0f}".format(time.time()),
               "counter" : counter
             }
    magnetoValues={
               "magneto": {                 
                   "compass": raw #"x: {x}, y: {y}, z: {z}".format(**raw)
               },
               "timestamp" : "{:.0f}".format(time.time()),
               "counter" : counter
             }
    msgs.append([{'topic': "pth", 'payload': json.dumps(pthValues)}, {'topic': "button", 'payload': json.dumps(buttonValues)}, {'topic': "magneto", 'payload': json.dumps(magnetoValues)}])      
    counter = counter + 1
    global temperatureTV,pressureTV,humidityTV,dir_xTV,dir_yTV,dir_zTV,lb_stickTV,pi_show
    temperatureTV.set(temperatureVar)
    pressureTV.set(pressureVar)
    humidityTV.set(humidityVar)
    dir_xTV.set(magnetoX)
    dir_yTV.set(magnetoY)
    dir_zTV.set(magnetoZ)
    lb_stickTV.set(dir)
    tx_subscribe=''
    print("pi_show: " + pi_show)
    if pi_show == '1':
      global tx_subscribe, showSubscribeMsgs
      tx_subscribe= temperatureVar + "C, " + pressureVar + "KPa, " + humidityVar + "%"
      lb_subscribeTV.set(tx_subscribe)
      showSubscribeMsgs.append(tx_subscribe)
      print("1showSubscribeMsgs[0]: " + showSubscribeMsgs[0])
      # sense.show_message(tx_subscribe)
    if pi_show == '2':
      tx_subscribe= dir
      lb_subscribeTV.set(tx_subscribe)
      showSubscribeMsgs.append(tx_subscribe)
      # sense.show_message(dir)
    if pi_show == '3':
      tx_subscribe= "x: {x}, y: {y}, z: {z}".format(**raw)
      lb_subscribeTV.set(tx_subscribe)
      showSubscribeMsgs.append(tx_subscribe)
      # sense.show_message(raw)
    
  # gui.humidity.config("text", humidity)
  # gui.temperature.config("text", temperature)
  # gui.dir_x.config("text", magnetoX)
  # gui.dir_y.config("text", magnetoY)
  # gui.dir_z.config("text", magnetoZ)
  # gui.lb_stick.config("text", dir)
  
    time.sleep(1)

def subscribeThread():
  global showSubscribeMsgs
  while True:
    if len(showSubscribeMsgs) >0:
      print("showSubscribeMsgs[0]: " + showSubscribeMsgs[0])
      sense.show_message(showSubscribeMsgs[0])
      showSubscribeMsgs.pop(0)

    time.sleep(1)
tls = {'cert_reqs': ssl.CERT_REQUIRED, 'tls_version': ssl.PROTOCOL_TLS}
auth = {'username': 'iot', 'password':'test123'}
# msgs = [{'topic': "pth", 'payload': json.dumps(pthValues)}, {'topic': "button", 'payload': json.dumps(buttonValues)}, {'topic': "magneto", 'payload': json.dumps(magnetoValues)}]
hostname = "mqtt.multisupport.co.nz" 
port = 8883
topics = "command"
pi_show=''
showSubscribeMsgs=[]
def loop_connect():
  global msgs, tls, auth, hostname
  while True:  
    if len(msgs) > 0:

      publish.multiple(msgs[0],hostname=hostname,port=port, auth=auth, tls=tls)
      dir = "none"
      msgs.pop(0)

def subscribeTopic():
  global msgs, tls, auth, hostname
  
  # The callback for when a PUBLISH message is received from the server.
  def on_message(client, userdata, msg):
      global pi_show,showSubscribeMsgs
      if pi_show != str(msg.payload):
        showSubscribeMsgs[:]=[]
        # sense.clear()
        pi_show = str(msg.payload)
        print(msg.topic+" "+str(msg.payload))
      


  client = mqtt.Client()
  client.on_connect = on_connect
  client.on_message = on_message

  client.tls_set(cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLS)
  client.username_pw_set("iot", password="test123")
  client.connect(host=hostname, port=port, keepalive=240)
  client.loop_forever()
  # Blocking call that processes network traffic, dispatches callbacks and
  # handles reconnecting.
  # Other loop*() functions are available that give a threaded interface and a
  # manual interface.
  
  # m = subscribe.simple(topics, hostname=hostname, retained=False, msg_count=1, port=port, qos=0, client_id="001", auth=auth, tls=tls, keepalive=0)     
  # # sense.show_message(m.payload)
  # print(m.payload)
  # lb_subscribeTV.set(m.payload)

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    global topics
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(topics)


threading.Thread(target=loop_getInfo).start()
threading.Thread(target=loop_connect).start()
threading.Thread(target=subscribeTopic).start()
threading.Thread(target=subscribeThread).start()

root.mainloop()
    
